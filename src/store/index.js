import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

var agroApi = axios.create({
    baseURL: 'https://localhost:44362/api/',
    //timeout: 1000,
    headers: {'Content-Type': 'application/json'}
  });
  

Vue.use(Vuex);

export default new Vuex.Store({
    state: () => ({
        user: { id: 1, name: 'User1', email: 'test@test.pl' },
        token: ""
    }),
    getters: {
        userName: state => {
            return state.user.name;
        },
        userEmail: state => {
            return state.user.email;
        }
    },
    
    mutations: {
        SET_USER (state, userData) {
            state.user = userData
        },
        SET_TOKEN (state, token) {
            //this.$cookie.set('auth-token', data);
            console.log('ustawiam token ' + token);
            state.token = token
        }
    },
    actions: {
        login({ commit }, userData){
            console.log(userData);
            agroApi.post('Account/Login', JSON.stringify(userData))
                .then(response => {
                    commit('SET_TOKEN', response.data.result);
                });
        },

        register({ commit }, userData){
            console.log(userData);
            agroApi.post('Account/Register',  JSON.stringify(userData))
                .then(response => {
                    commit('SET_TOKEN', response.data.result);
                    console.log(response)
                })
                .catch(error => {
                    console.log(error);
                });
            
        }
    },
    modules: {}
})